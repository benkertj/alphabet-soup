#Alphabet Soup Solution - Written in Python

- Author: Jason Benkert
- Creation Date: 11/22/2021

###Background
This project reflects a solution written in Python in addressing the Alphabet Soup problem described here:

[https://gitlab.com/enlighten-challenge/alphabet-soup](https://gitlab.com/enlighten-challenge/alphabet-soup)

The goals of my solution are to:
- Be flexible in handling small to large files as well user provided files
- Be performant by parsing large matrices and returning possible start and end coordinate values in a responsive manner 
- Handle edge cases within the inputted data file and report appropriately
- Ensure solution is comprensible by the reviewer
- Ensure solution can be built and packaged (e.g., pip)


The logic behind the search function is to find all possible cells that equal the first character of the search string via numpy.where function. When there is a match the program begins to look in all 8 directions from the current cell (up,down,left,right,left-top diagonal,right-top diagonal, left-bottom diagonal, right-bottom diagonal). For each direction it calculate's the end character of the search string's coordinates. It then verifies that the current cell and the last character's cell fall within the matrix dimensions. If both coordinates fall within the dimensions then it verifies the matrix characters in that particular direction against the search string. If there is a match it will return the start and end coordinates of that string. If there are no matches it will continue to loop through all remaining possible directions and move the cursor onto the next character in the matrix until there is a search string match or the end of the matrix is reached. If no search string is found in the matrix it will return 'None'

###Assumptions
- Case-sensitive comparison between search keyword character and matrix character
- If the search string is not found in the matrix 'None' will be returned next to search string.
- And finally, if the search string is in the matrix that there is only 1 instance of it.

###System Dependencies
- Python3+ (Required)

###Program  Dependencies
Next install the program dependencies required. From the top-level directory run the following command:

```
pip3 install -r requirements.txt
```

### Run Program
Run the Python program from the top-level directory via:

```
python3 src/Main.py
```

The main class will run through 4 different matrices (2 that were described in the initial problem definition; and 2 others that I created to identify edge cases) and output the corresponding keyword strings that were found in the crossword puzzle, if any, and their associated initial and ending index coordinates. Also, a Completion Time metric output (in seconds) was added to demonstrate to the reviewer how long it took to parse the file and perform the search of the matrix for each of the keywords specified.

If the user desires to provide their own data file please pass in the following argument:

```
python3 src/Main.py --file <fully qualified path of file>
```

Example:

```
python3 src/Main.py --file /home/jdoe/alphSoup.txt
```

### Run Unit Tests
In order to run the Python unit-level tests please run the following command at the top-level of the project:

```
python3 -m pytest
```

There are 10 unit tests that were developed to check for normal cases and edge cases in the search logic and parsing of data files. 6 of the unit tests are checking assertions within search logic and the remaining unit tests are checking assertions in parsing the data files.

### Create Package
To create a build package you'll need to run the following command at the top-level directory:

```
python3 -m build
```

The package will be generated and placed under the dist/ folder at the root-level of the project.