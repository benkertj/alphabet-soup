#!/usr/bin/env python3

##//////////////////////////////////////////////////////////////////////////////
# %%DSR_COPYRIGHT_INFO%% #
##//////////////////////////////////////////////////////////////////////////////

##
## Author: Jason Benkert
## Creation Date: 22 November 2021
## Class: Utility Test Class for asserting the value outputs for different functions.
##//////////////////////////////////////////////////////////////////////////////

import sys
import unittest
sys.path.append('../src')
from RawFileParser import RawFileParser
from WordSearchSolver import WordSearchSolver

class TestUtility(unittest.TestCase):
    def setUp(self):
        self.parser = RawFileParser()
        self.solver = WordSearchSolver()

    def test_3x3_matrix_success_1st_String(self):
      """
      Test that verifies 3x3 matrix with success in finding 1st String
      """
      matrixSize = [3,3]
      matrix = [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I']]
      strings = ['ABC']
      result = self.solver.stringSearch(strings[0], matrixSize, matrix)
      self.assertEqual('{}:{} {}:{}'.format(result[0], result[1], result[2], result[3]), "0:0 0:2")


    def test_3x3_matrix_success_2nd_String(self):
      """
      Test that verifies 3x3 matrix with success in finding 1st String
      """
      matrixSize = [3,3]
      matrix = [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I']]
      strings = ['AEI']
      result = self.solver.stringSearch(strings[0], matrixSize, matrix)
      self.assertEqual('{}:{} {}:{}'.format(result[0], result[1], result[2], result[3]), "0:0 2:2")
  
    def test_3x4_matrix_failure_1st_String(self):
      """
      Test that verifies 3x4 matrix with failure in finding 1st String
      """
      matrixSize = [3,4]
      matrix = [['A', 'B', 'C', 'T'], ['D', 'E', 'F', 'F'], ['G', 'H', 'I', 'D']]
      strings = ['TFD']
      result = self.solver.stringSearch(strings[0], matrixSize, matrix)
      self.assertEqual('{}:{} {}:{}'.format(result[0], result[1], result[2], result[3]), "0:3 2:3")

    def test_5x5_matrix_success_1st_String(self):
      """
      Test that verifies 5x5 matrix with success in finding 1st String
      """
      matrixSize = [5,5]
      matrix = [['H', 'A', 'S', 'D', 'F'], ['G', 'E', 'Y', 'B', 'H'], ['J', 'K', 'L', 'Z', 'X'], ['C', 'V', 'B', 'L', 'N'], ['G', 'O', 'O', 'D', 'O']]
      strings = ['HELLO']
      result = self.solver.stringSearch(strings[0], matrixSize, matrix)
      self.assertEqual('{}:{} {}:{}'.format(result[0], result[1], result[2], result[3]), "0:0 4:4")

    def test_5x5_matrix_success_2nd_String(self):
      """
      Test that verifies 5x5 matrix with success in finding 2nd String
      """
      matrixSize = [5,5]
      matrix = [['H', 'A', 'S', 'D', 'F'], ['G', 'E', 'Y', 'B', 'H'], ['J', 'K', 'L', 'Z', 'X'], ['C', 'V', 'B', 'L', 'N'], ['G', 'O', 'O', 'D', 'O']]
      strings = ['GOOD']
      result = self.solver.stringSearch(strings[0], matrixSize, matrix)
      self.assertEqual('{}:{} {}:{}'.format(result[0], result[1], result[2], result[3]), "4:0 4:3")

    def test_5x5_matrix_failure_3rd_String(self):
      """
      Test that verifies 5x5 matrix with failure in finding 3rd String
      """
      matrixSize = [5,5]
      matrix = [['H', 'A', 'S', 'T', 'F'], ['G', 'E', 'Y', 'B', 'H'], ['J', 'K', 'L', 'Z', 'X'], ['C', 'V', 'B', 'L', 'N'], ['G', 'O', 'O', 'D', 'O']]
      strings = ['BAM']
      result = self.solver.stringSearch(strings[0], matrixSize, matrix)
      self.assertEqual(result, None)

    def test_process_file_3x3(self):
      """
      Test that verifies 3x3 file can be parsed correctly
      """
      [size, matrix, searchStrings] = self.parser.processDataFile("./resources/3x3-cases-valid.txt")
      self.assertEqual(size, [3,3])
      self.assertEqual(matrix, [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I']] )
      self.assertEqual(searchStrings, ['ABC', 'AEI', 'CEG', 'A'])

    def test_process_file_3x4(self):
      """
      Test that verifies 3x4 file can be parsed correctly
      """
      [size, matrix, searchStrings] = self.parser.processDataFile("./resources/3x4-cases-valid.txt")
      self.assertEqual(size, [3,4])
      self.assertEqual(matrix, [['A', 'B', 'C', 'T'], ['D', 'E', 'F', 'F'], ['G', 'H', 'I', 'D']] )
      self.assertEqual(searchStrings, ['TFD', 'HID', 'CFI', 'G'])

    def test_process_file_5x5(self):
      """
      Test that verifies 5x5 file can be parsed correctly
      """
      [size, matrix, searchStrings] = self.parser.processDataFile("./resources/5x5-cases-valid.txt")
      self.assertEqual(size, [5,5])
      self.assertEqual(matrix, [['H', 'A', 'S', 'D', 'F'], ['G', 'E', 'Y', 'B', 'H'], ['J', 'K', 'L', 'Z', 'X'], ['C', 'V', 'B', 'L', 'N'], ['G', 'O', 'O', 'D', 'O']] )
      self.assertEqual(searchStrings, ['HELLO', 'GOOD', 'BYE'])

    def test_process_file_20x20(self):
      """
      Test that verifies 20x20 file can be parsed correctly
      """
      [size, matrix, searchStrings] = self.parser.processDataFile("./resources/20x20-cases-valid.txt")
      self.assertEqual(size, [20,20])
      self.assertEqual(matrix, [['H', 'A', 'S', 'D', 'F', 'M', 'T', 'W', 'I', 'O', 'P', 'Q', 'R', 'T', 'B', 'E', 'T', 'B', 'Z', 'I'], 
          ['G', 'E', 'Y', 'B', 'H', 'L', 'K', 'E', 'U', 'M', 'N', 'P', 'T', 'N', 'E', 'T', 'L', 'K', 'T', 'B'], 
          ['J', 'K', 'L', 'Z', 'X', 'K', 'E', 'B', 'O', 'M', 'W', 'R', 'L', 'B', 'P', 'U', 'V', 'W', 'Q', 'B'], 
          ['C', 'V', 'B', 'L', 'N', 'K', 'B', 'Q', 'A', 'Z', 'M', 'N', 'P', 'O', 'Q', 'B', 'T', 'L', 'K', 'B'], 
          ['G', 'O', 'O', 'D', 'O', 'K', 'J', 'T', 'W', 'P', 'M', 'N', 'P', 'N', 'L', 'O', 'P', 'Q', 'J', 'B'], 
          ['A', 'D', 'A', 'D', 'D', 'K', 'K', 'A', 'N', 'T', 'A', 'A', 'E', 'Q', 'P', 'T', 'B', 'W', 'L', 'M'], 
          ['A', 'D', 'K', 'T', 'A', 'L', 'T', 'A', 'P', 'M', 'B', 'M', 'N', 'P', 'O', 'P', 'T', 'R', 'L', 'T'], 
          ['M', 'D', 'A', 'L', 'L', 'P', 'M', 'W', 'T', 'A', 'L', 'D', 'K', 'T', 'W', 'E', 'T', 'F', 'V', 'T'], 
          ['A', 'T', 'T', 'A', 'M', 'B', 'N', 'P', 'O', 'R', 'R', 'T', 'W', 'L', 'T', 'W', 'B', 'W', 'T', 'L'], 
          ['P', 'B', 'W', 'T', 'A', 'W', 'B', 'N', 'T', 'O', 'B', 'W', 'T', 'R', 'W', 'K', 'D', 'B', 'T', 'L'], 
          ['J', 'A', 'K', 'D', 'K', 'A', 'B', 'W', 'T', 'A', 'W', 'A', 'B', 'T', 'L', 'D', 'A', 'B', 'T', 'A'], 
          ['T', 'W', 'L', 'M', 'N', 'B', 'W', 'O', 'P', 'W', 'T', 'R', 'L', 'T', 'M', 'M', 'D', 'A', 'A', 'T'], 
          ['M', 'D', 'A', 'T', 'L', 'W', 'M', 'B', 'W', 'L', 'D', 'A', 'P', 'P', 'W', 'L', 'W', 'L', 'K', 'T'], 
          ['W', 'E', 'E', 'T', 'M', 'M', 'A', 'D', 'T', 'T', 'A', 'W', 'L', 'B', 'E', 'T', 'F', 'F', 'W', 'T'], 
          ['T', 'W', 'E', 'T', 'R', 'L', 'D', 'B', 'W', 'L', 'D', 'A', 'E', 'T', 'T', 'R', 'W', 'W', 'R', 'B'], 
          ['B', 'B', 'B', 'W', 'L', 'W', 'E', 'T', 'R', 'A', 'W', 'B', 'W', 'E', 'T', 'R', 'W', 'E', 'T', 'B'], 
          ['D', 'A', 'D', 'E', 'W', 'R', 'R', 'A', 'W', 'T', 'R', 'A', 'W', 'E', 'T', 'R', 'A', 'A', 'E', 'T'], 
          ['A', 'D', 'B', 'W', 'E', 'T', 'W', 'T', 'A', 'W', 'T', 'R', 'B', 'Y', 'R', 'W', 'E', 'M', 'M', 'N'], 
          ['T', 'W', 'W', 'L', 'N', 'O', 'P', 'W', 'R', 'Y', 'V', 'W', 'L', 'W', 'T', 'A', 'B', 'D', 'A', 'R'], 
          ['A', 'T', 'A', 'L', 'D', 'A', 'M', 'N', 'A', 'T', 'P', 'R', 'R', 'B', 'W', 'W', 'K', 'D', 'T', 'R']] )
      self.assertEqual(searchStrings, ['BBBB', 'PMN', 'RAMA'])

if __name__ == '__main__':
  unittest.main()