#!/usr/bin/env python3

##//////////////////////////////////////////////////////////////////////////////
# %%DSR_COPYRIGHT_INFO%% #
##//////////////////////////////////////////////////////////////////////////////

##
## Author: Jason Benkert
## Creation Date: 22 November 2021
## Class: Search class for Alphabet Soup matrices in scanning for specified strings.  
##//////////////////////////////////////////////////////////////////////////////

import itertools
import numpy as np

class WordSearchSolver:

    def __init__(self):
      #Initialize all possible permutations besides [0,0] for possible directions 
      self._DIRECTION = np.array([t for t in list(itertools.product([-1, 0, 1], repeat=2)) if t[0] != 0 or t[1] != 0])
      self._ROW = 0
      self._COL = 0

    # Searches the matrix in all 8 directions from first letter
    def searchMatrix(self, word, row, col, matrix):

      #Determine word length
      wordLen = len(word)
  
      # Check first character of word to ensure it doesn't match starting point in matrix.
      if matrix[row][col] != word[0]:
        return False
    
      #Check for single character string
      if wordLen == 1:
        return (str(row),str(col))

      # Search word in all 8 directions upon match of first character
      for dir in self._DIRECTION:
        
        # Initialize last character of word's ending point
        lastX = 0
        lastY = 0
        #Initialize search string match check
        stringMatch = True
        # Initialize starting point for current direction
        cursorX = row + dir[0]
        cursorY = col + dir[1]

        # Last character coordinate of string based on cursor and direction
        endWordCoord = [dir[0] * (wordLen - 1) + row, dir[1] * (wordLen - 1) + col]

        # Check whether first character and last character of string fall within matrix dimensions
        if 0 <= cursorX < self._ROW and \
          0 <= cursorY < self._COL and \
          0 <= endWordCoord[0] < self._ROW and \
          0 <= endWordCoord[1] < self._COL:

            # First character is already checked, match remaining characters now that matrix dimensions are verified
            for c in np.arange(1, wordLen):
                # Verify it is correct string
                if (word[c] == matrix[cursorX][cursorY]):
                    lastX = cursorX
                    lastY = cursorY
                    # Move cursor
                    cursorX += dir[0]
                    cursorY += dir[1]
                else:
                    stringMatch = False
                    break
            if stringMatch:
                return (str(lastX), str(lastY))
      return False

    # Searches for string in a given matrix
    def stringSearch(self, word, size, matrix):
  
      # Define row and column size for given matrix
      self._ROW = size[0]
      self._COL = size[1]
      
      numpyMatrix = np.asarray(matrix)
      locations = np.where(numpyMatrix == word[0])
      locations = [list(k) for k in zip(locations[0], locations[1])]
      for entry in locations:
        value = self.searchMatrix(word, entry[0], entry[1], numpyMatrix)
        if value:
          return (str(entry[0]), str(entry[1]), str(value[0]), str(value[1]))
      return None