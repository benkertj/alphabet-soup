#!/usr/bin/env python3

##//////////////////////////////////////////////////////////////////////////////
# %%DSR_COPYRIGHT_INFO%% #
##//////////////////////////////////////////////////////////////////////////////

##
## Author: Jason Benkert
## Creation Date: 22 November 2021
## Class: Main class for processing data file and searching matrix
##//////////////////////////////////////////////////////////////////////////////

import argparse
import sys
sys.path.append('./src')
import timeit
from RawFileParser import RawFileParser
from WordSearchSolver import WordSearchSolver

if __name__ == "__main__":
    fileParser = RawFileParser()
    solver = WordSearchSolver()
    argParser = argparse.ArgumentParser()
    argParser.add_argument("--file", "-a", type=str, required=False, help="Add data input file")
    args = argParser.parse_args()
    print()
    # Check to see if using operator referenced data file or internal data files.
    if(args.file is not None):
        print("### Processing User Inputed Data File ###")
        start = timeit.default_timer()
        [size, matrix, searchStrings] = fileParser.processDataFile(args.file)
        for index,word in enumerate(searchStrings):
            result = solver.stringSearch(word, size, matrix)
            if result[0] == "None": 
                print('{} {}'.format(word, result[0]))
            else:
                print('{} {}:{} {}:{}'.format(word, result[0], result[1], result[2], result[3]))
        end = timeit.default_timer()
        print( "Completion Time: " + str(round(end - start, 7)) + " second" )

    else:
        #Process 3x3 Valid File
        print("### Processing 3x3 Matrix - Valid ###")
        start = timeit.timeit()
        [size, matrix, searchStrings] = fileParser.processDataFile("./resources/3x3-cases-valid.txt")
        for index,word in enumerate(searchStrings):
            result = solver.stringSearch(word, size, matrix)
            if result is None: 
                print('{} {}'.format(word, "None"))
            else:
                print('{} {}:{} {}:{}'.format(word, result[0], result[1], result[2], result[3]))
        end = timeit.default_timer()
        print( "Completion Time: " + str(round(end - start, 7)) + " second" )

        #Process 5x5 Valid File
        print("\n### Processing 5x5 Matrix - Valid ###")
        start = timeit.default_timer()
        [size, matrix, searchStrings] = fileParser.processDataFile("./resources/5x5-cases-valid.txt")
        for index,word in enumerate(searchStrings):
            result = solver.stringSearch(word, size, matrix)
            if result is None: 
                print('{} {}'.format(word, "None"))
            else:
                print('{} {}:{} {}:{}'.format(word, result[0], result[1], result[2], result[3]))
        end = timeit.default_timer()
        print( "Completion Time: " + str(round(end - start, 7)) + " second" )
