#!/usr/bin/env python3

##//////////////////////////////////////////////////////////////////////////////
# %%DSR_COPYRIGHT_INFO%% #
##//////////////////////////////////////////////////////////////////////////////

##
## Author: Jason Benkert
## Creation Date: 22 November 2021
## Class: File parser class for Alphabet Soup raw data files
##//////////////////////////////////////////////////////////////////////////////

import numpy as np

class RawFileParser:

    # Processes data file to determine matrix size, matrix, and search strings
    def processDataFile(self,filename):
      matrixSize = []
      matrix = []
      searchWords = []

      try:
        matrixCol = []
        with open(filename, "r") as f:
            for index, line in enumerate(f):
                strippedLine = line.strip()
                if index == 0:
                    matrixSize = [ int(val) for val in strippedLine.split("x") ];
                elif index >= 1 and index < (int(matrixSize[0]) + 1):
                    matrixCol = strippedLine.split(" ")
                    matrix.append(matrixCol)
                else:
                    searchWords.append(strippedLine.replace(" ", ""))
        
        if(len(matrixSize) != 2):
          print("Corrupt data file: matrix size is not 2-dimensional. Matrix size is " + str(len(matrixSize)))

        if(matrixSize[0] != len(matrix) and matrixSize[1] != len(matrixCol)):
          print("Corrupt data file: defined matrix size (" + str(matrixSize[0]) + ":" + str(matrixSize[1]) + 
            ") does not equal actual matrix size(" + str(len(matrix)) + ":" + str(len(matrixCol)) + ")")

        if(len(searchWords) == 0):
          print("Corrupt data file: no search strings found")

        return (matrixSize, matrix, searchWords)
      except Exception as inst:
        print("Error processing file: " + str(type(inst)))